﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GraveLabs.GDP.Seek.Core.Test
{
    [TestClass]
    public class CheckoutTest
    {
        [TestMethod]
        public void DefaultCustomerTest()
        {
            var checkout = new Checkout();
            var skus = new[] { "classic", "standout", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(987.97m, checkout.Total());
        }

        [TestMethod]
        public void CustomerWithInvalidSkuTest()
        {
            var checkout = new Checkout();
            var skus = new[] { "classic", "", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(664.98m, checkout.Total());
        }

        [TestMethod]
        public void UnileverCustomerWithDealTest()
        {
            var checkout = new Checkout("unilever");
            var skus = new[] { "classic", "classic", "classic", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(934.97m, checkout.Total());
        }
        [TestMethod]
        public void UnileverCustomerWithoutDealTwoClassicTest()
        {
            var checkout = new Checkout("unilever");
            var skus = new[] { "classic", "classic"};
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(539.98m, checkout.Total());
        }
        [TestMethod]
        public void UnileverCustomerWithoutDealTest()
        {
            var checkout = new Checkout("unilever");
            var skus = new[] { "standout", "standout", "standout", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(1363.96m, checkout.Total());
        }
        [TestMethod]
        public void AppleCustomerWithDealTest()
        {
            var checkout = new Checkout("apple");
            var skus = new[] { "standout", "standout", "standout", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(1294.96m, checkout.Total());
        }
        [TestMethod]
        public void AppleCustomerWithoutDealTest()
        {
            var checkout = new Checkout("apple");
            var skus = new[] { "classic", "classic", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(934.97m, checkout.Total());
        }
        [TestMethod]
        public void NikeCustomerWithDealTest()
        {
            var checkout = new Checkout("nike");
            var skus = new[] { "premium", "premium", "premium", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(1519.96m, checkout.Total());
        }

        [TestMethod]
        public void NikeCustomerWithoutDealTest()
        {
            var checkout = new Checkout("nike");
            var skus = new[] { "premium", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(789.98m, checkout.Total());
        }
        [TestMethod]
        public void CustomerWithAllDealsTest()
        {
            var checkout = new Checkout("ford");
            var skus = new[] { "classic", "classic", "classic", "classic", "classic", "standout", "premium", "premium", "premium" };
            foreach (var sku in skus)
            {
                checkout.Add(sku);
            }
            Assert.AreEqual(2559.92m, checkout.Total());
        }
    }
}
