﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraveLabs.GDP.Seek.Core.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraveLabs.GDP.Seek.Core.Model;

namespace GraveLabs.GDP.Seek.Core.Providers.Tests
{
    [TestClass()]
    public class JsonProviderTests
    {
        [TestMethod()]
        public void GetDefaultAdsTest()
        {
            var path = @"Data\ads.json";
            var provider = new JsonProvider();
            var data = provider.GetData<Ad>(path);
            Assert.AreEqual(data.Count, 3);
        }

        [TestMethod()]
        public void GetRulesTest()
        {
            var path = @"Data\rules.json";
            var provider = new JsonProvider();
            var data = provider.GetData<Rule>(path);
            Assert.AreEqual(data.Count, 6);
        }
    }
}