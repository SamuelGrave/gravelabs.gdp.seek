﻿using System;
using System.Collections.Generic;
using System.Linq;
using GraveLabs.GDP.Seek.Core.Model;
using GraveLabs.GDP.Seek.Core.Providers;

namespace GraveLabs.GDP.Seek.Core.Repository
{
    public class RulesRepository : IRepository<Rule>
    {
        private readonly IList<Rule> _rules;
        private string _path = @"Data\rules.json";
        public RulesRepository(IProvider provider)
        {
            _rules = provider.GetData<Rule>(_path);
        }
        public Rule Retrieve(object id)
        {
            return _rules.FirstOrDefault(x => x.Id.Equals(id));
        }

        public IList<Rule> RetrieveAll()
        {
            return _rules;
        }

        public IList<Rule> RetrieveAll(string customer)
        {
            return _rules.Where(x => x.Customer.Equals(customer, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }
    }
}