﻿using System.Collections.Generic;
using System.Linq;
using GraveLabs.GDP.Seek.Core.Model;
using GraveLabs.GDP.Seek.Core.Providers;

namespace GraveLabs.GDP.Seek.Core.Repository
{
    public class AdRepository : IRepository<Ad>
    {
        private readonly IList<Ad> _ads;
        private string _path = @"Data\ads.json";
        public AdRepository(IProvider provider)
        {
            _ads = provider.GetData<Ad>(_path);
        }
        public Ad Retrieve(object id)
        {
            return _ads.FirstOrDefault(x => x.Id.Equals(id));
        }

        public IList<Ad> RetrieveAll()
        {
            return _ads;
        }
    }
}