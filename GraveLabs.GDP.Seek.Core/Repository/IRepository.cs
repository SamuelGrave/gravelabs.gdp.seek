﻿using System.Collections.Generic;

namespace GraveLabs.GDP.Seek.Core.Repository
{
    public interface IRepository<T>
    {
        T Retrieve(object id);
        IList<T> RetrieveAll();
    }
}