﻿namespace GraveLabs.GDP.Seek.Core.Model
{
    public class Rule
    {
        public int Id { get; set; }
        public string Customer { get; set; }
        public string AdType { get; set; }
        public RuleTypeEnum RuleType { get; set; }
        public string[] Params { get; set; }
    }
}