﻿namespace GraveLabs.GDP.Seek.Core
{
    public enum RuleTypeEnum
    {
        XForYDeal,
        Discount,
        DiscountWhen
    }
}