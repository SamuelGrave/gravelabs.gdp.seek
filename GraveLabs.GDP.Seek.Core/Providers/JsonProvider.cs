﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace GraveLabs.GDP.Seek.Core.Providers
{
    public class JsonProvider : IProvider
    {
        public IList<T> GetData<T>(string path)
        {
            using (var sr = new StreamReader(path))
            {
                var json = sr.ReadToEnd();
                return JsonConvert.DeserializeObject<List<T>>(json);
            }
        }
    }
}