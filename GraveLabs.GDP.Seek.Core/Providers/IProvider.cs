﻿using System.Collections.Generic;

namespace GraveLabs.GDP.Seek.Core.Providers
{
    public interface IProvider
    {
        IList<T> GetData<T>(string path);
    }
}