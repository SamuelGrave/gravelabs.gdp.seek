﻿using GraveLabs.GDP.Seek.Core.RuleSets;

namespace GraveLabs.GDP.Seek.Core
{
    public class Checkout
    {
        private IRuleSet _ruleSet;
        
        public Checkout()
        {
            _ruleSet = new RuleSet("default");
        }

        public Checkout(string customer)
        {
            _ruleSet = new RuleSet(customer);
        }
        public void Add(string id)
        {
            _ruleSet.Add(id);
        }

        public decimal Total()
        {
            return _ruleSet.Total();
        }
    }
}