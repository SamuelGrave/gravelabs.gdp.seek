﻿using System.Collections;
using System.Collections.Generic;
using GraveLabs.GDP.Seek.Core.Model;

namespace GraveLabs.GDP.Seek.Core
{
    public interface IRule
    {
        decimal Total(IEnumerable<Ad>ads);
    }
}