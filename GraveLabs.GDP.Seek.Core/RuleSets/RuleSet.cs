﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using GraveLabs.GDP.Seek.Core.Model;
using GraveLabs.GDP.Seek.Core.Providers;
using GraveLabs.GDP.Seek.Core.Repository;

namespace GraveLabs.GDP.Seek.Core.RuleSets
{
    public class RuleSet : IRuleSet
    {
        private List<Ad> _ads;
        private IRepository<Ad> _repository;
        private List<Rule> _rules;
        private static CultureInfo _ci = CultureInfo.InvariantCulture;
        public RuleSet(string customer)
        {
            _repository = new AdRepository(new JsonProvider());
            _ads = new List<Ad>();
            var rulesRepository = new RulesRepository(new JsonProvider());
            _rules = rulesRepository.RetrieveAll(customer).ToList();
        }
        public void Add(string adId)
        {
            var ad = _repository.Retrieve(adId);
            if (ad == null) return;
            _ads.Add(ad);
        }

        public decimal Total()
        {
            if (_rules?.Count == 0) return _ads.Sum(x => x.Price);
            var total = 0m;
            total += GetAdsTotalFor(_ads.Where(x => x.Id.Equals("classic")), _rules.FirstOrDefault(x => x.AdType.Equals("classic")));
            total += GetAdsTotalFor(_ads.Where(x => x.Id.Equals("standout")), _rules.FirstOrDefault(x => x.AdType.Equals("standout")));
            total += GetAdsTotalFor(_ads.Where(x => x.Id.Equals("premium")), _rules.FirstOrDefault(x => x.AdType.Equals("premium")));

            return total;
        }

        private decimal GetAdsTotalFor(IEnumerable<Ad> ads, Rule rule)
        {
            if (!ads.Any()) return 0m;
            if (rule == null) return ads.Sum(x => x.Price);
            var total = 0m;
            switch (rule.RuleType)
            {
                case RuleTypeEnum.XForYDeal:
                    total = CalculateXForYDeal(ads, rule);
                    break;
                case RuleTypeEnum.Discount:
                    total = CalculateDiscount(ads, rule);
                    break;
                case RuleTypeEnum.DiscountWhen:
                    total = CalculateDiscountWhen(ads, rule);
                    break;
            }
            return total;
        }

        private static decimal CalculateDiscountWhen(IEnumerable<Ad> ads, Rule rule)
        {
            var moreThan = Convert.ToInt32(rule.Params[0]);
            var price = Convert.ToDecimal(rule.Params[1], _ci);
            if (ads.Count() >= moreThan) return ads.Count() * price;
            return ads.Sum(x => x.Price);
        }

        private static decimal CalculateDiscount(IEnumerable<Ad> ads, Rule rule)
        {
            var price = Convert.ToDecimal(rule.Params[0], _ci);
            return ads.Count() * price;
        }
        private static decimal CalculateXForYDeal(IEnumerable<Ad> ads, Rule rule)
        {
            var getX = Convert.ToInt32(rule.Params[0]);
            var payY = Convert.ToInt32(rule.Params[1]);
            var price = ads.FirstOrDefault().Price;
            var qtd = ads.Count();
            return ((qtd / getX) * payY + (qtd % getX)) * price;
        }
    }
}