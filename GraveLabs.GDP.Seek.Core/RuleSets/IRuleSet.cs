﻿namespace GraveLabs.GDP.Seek.Core.RuleSets
{
    public interface IRuleSet
    {
        void Add(string adId);
        decimal Total();
    }
}