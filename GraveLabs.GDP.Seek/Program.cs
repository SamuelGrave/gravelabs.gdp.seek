﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraveLabs.GDP.Seek.Core;

namespace GraveLabs.GDP.Seek
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Customer:");
            var company = Console.ReadLine();
            Console.WriteLine("SKUs:");
            var skus = Console.ReadLine();
            if (string.IsNullOrEmpty(skus)) return;
            var ads = skus.Split(',');
            var checkout = new Checkout();
            for (int i = 0; i < ads.Length; i++)
            {
                checkout.Add(ads[i]);
            }
            Console.WriteLine($"Total expected: ${checkout.Total()}");
            Console.ReadLine();
        }
    }
}
